package main

// FileStore represents a service that stores data.
type FileStore interface {
	// Put puts data into the file store, if the file store only supports
	// ascii / utf8 then data should be encoded and decoded by Get and Put
	Put(data []byte) (link string, err error)

	// Get file from the file store from it's link.
	Get(link string) ([]byte, error)

	// Name of the service (used for debugging).
	Name() string
}

// FileStores contains all the implemented FileStore's
var FileStores = [...]FileStore{
	Termbin{},
	Pastebin{"42d8c13038f701723cf3d145fd6cc08b"},
}
