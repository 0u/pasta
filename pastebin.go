package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

// Pastebin implements the FileStore interface for pastebin.
type Pastebin struct {
	APIKey string
}

// Name returns the name of this service, in this case "pastebin"
func (t Pastebin) Name() string { return "pastebin" }

// Put puts some data to pastebin using the provided API key.
// It returns the human visitable link, the Get method will convert this to
// a raw link when it is fetching the data.
func (t Pastebin) Put(data []byte) (link string, err error) {
	options := url.Values{}

	options.Set("api_dev_key", t.APIKey)
	options.Set("api_option", "paste")
	options.Set("api_paste_code", string(data))

	resp, err := http.PostForm("https://pastebin.com/api/api_post.php", options)
	if err != nil {
		return "", err
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	// Inline function to process the rare case of "Post Limit" error
	// Looks like: Post%20limit,%20maximum%20pastes%20per%2024h%20reached
	urlFilter := func(url string) string {
		return strings.Join(strings.Split(url, "%20"), " ")
	}

	if strings.Contains(string(body), "Bad API request") {
		return "", errors.New(string(body))
	} else if strings.Contains(urlFilter(string(body)), "Post limit") {
		return "", errors.New(urlFilter(string(body)))
	}

	return string(body), nil
}

// Get the contents of a paste from pastebin, if it does not contain /raw/ then
// /raw/ is prepended to the path.
func (t Pastebin) Get(link string) ([]byte, error) {
	pasteURL, err := url.Parse(link)
	if err != nil {
		return nil, err
	}

	// If the url does not contain /raw/ before the paste id prepend it.
	if !strings.Contains("/raw/", pasteURL.Path) {
		pasteURL.Path = "/raw" + pasteURL.Path
	}

	fmt.Println("Getting data back from: ", pasteURL.String())
	resp, err := http.Get(pasteURL.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}
