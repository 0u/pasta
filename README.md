# pasta

**Under construction**

Pasta is a way to securely store large files in a series of pastebin sites.


It works by spliting the file into chunks that can fit into pastebin sites, then encrypts them with aes256 gcm
and uploads them.

then it creates a single `index.json` file that contains all the links to chunks needed to redownload the file.
