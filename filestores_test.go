package main

import (
	"bytes"
	"crypto/rand"
	"testing"
)

func TestFileStores(t *testing.T) {
	for _, fs := range FileStores {
		t.Run(fs.Name(), func(t *testing.T) {
			t.Parallel()
			// Get some random data for the test
			want := make([]byte, 16)
			_, err := rand.Read(want)
			if err != nil {
				t.Fatal(err)
			}

			// Put the data to the file store.
			link, err := fs.Put(want)
			if err != nil {
				t.Fatal(err)
			}
			t.Logf("Posted test data to %s", link)

			// Get the data back.
			got, err := fs.Get(link)

			// Check the length before the contents, much easier for debugging.

			lg, lw := len(got), len(want)
			if lg != lw {
				t.Logf("got: %X, want: %X", got, want)
				t.Fatalf("len(got) = %d, len(want) = %d", lg, lw)
			}

			if !bytes.Equal(got, want) {
				t.Fatalf("want %X, got %X", want, got)
			}
		})
	}
}
