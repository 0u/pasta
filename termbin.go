package main

import (
	"bytes"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
)

// Termbin implements the FileStore interface for termbin.com
type Termbin struct{}

// Name returns the name of this service, in this case "termbin"
func (t Termbin) Name() string { return "termbin" }

// Put stores data on termbin by connecting via tcp, sending data then receiving the link.
func (t Termbin) Put(data []byte) (link string, err error) {
	c, err := net.Dial("tcp", "termbin.com:9999")
	if err != nil {
		return "", err
	}
	conn := c.(*net.TCPConn)

	if _, err := conn.Write(data); err != nil {
		return "", err
	}

	if err := conn.CloseWrite(); err != nil {
		return "", err
	}

	// 64 should be plenty for a url.
	buf := make([]byte, 64)
	n, err := conn.Read(buf)
	if err != nil {
		return "", err
	}
	conn.CloseRead()

	// Was having an issue with the url ending in \n\x00, this fixes it.
	link = string(bytes.Trim(buf[:n], "\x00\n\r "))
	if _, err := url.Parse(link); err != nil {
		return "", err
	}
	return link, nil
}

// Get is simple, because termbin links are always raw.
func (t Termbin) Get(link string) ([]byte, error) {
	resp, err := http.Get(link)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	return ioutil.ReadAll(resp.Body)
}
